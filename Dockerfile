FROM nginx:alpine

RUN apk add --update --no-cache fcgiwrap spawn-fcgi

COPY "./config/fcgiwrap.conf" "/etc/nginx/fcgiwrap.conf"
COPY "./config/nginx-fcgi.conf" "/etc/nginx/conf.d/default.conf"
COPY "./config/startup.sh" "/startup.sh"
COPY "./bin" "/var/www/cgi-bin"

CMD ["/bin/sh", "-c", "./startup.sh"]
