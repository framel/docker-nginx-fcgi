## Docker Nginx FCGI

This repository contains **configuration** to run [Nginx](http://nginx.org/) with [fcgiwrap](https://github.com/gnosek/fcgiwrap) and [spawn-fcgi](https://github.com/lighttpd/spawn-fcgi) for **FCGI** support inside [Docker](https://www.docker.com/) container based on [Alpine Linux](https://www.alpinelinux.org/).

### Build image

``` sh
docker build -t nginx-fcgi .
```

### Instantiate container

``` sh
docker run -d -p 80:80 --name fcgi-container -v $PWD/data:/data nginx-fcgi
```

### Upload file

``` sh
curl -X POST http://127.0.0.1/cgi-bin/upload?saved_file.txt --data-binary @'source_file.txt'
```
*Source codes of CGI binaries are available [here](https://framagit.org/framel/cgi-scripts).*

### Stop container

Send SIGTERM signal to fcgiwrap with:
``` sh
sh fast_stop.sh
```

#### Acknowledgment
*Freely inspire from Harald Westphal [Nginx FCGI Dockerfile](https://github.com/hwestphal/docker-nginx-fcgi).*
