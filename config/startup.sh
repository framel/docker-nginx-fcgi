#!/bin/sh

chmod 777 /var/www/cgi-bin
nginx
spawn-fcgi -n -u nginx -g www-data -s /var/run/fcgiwrap.socket /usr/bin/fcgiwrap
