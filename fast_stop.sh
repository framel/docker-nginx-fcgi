#!/bin/sh

FCGI_PID="$(docker exec fcgi-container pgrep fcgiwrap)"
docker exec fcgi-container kill -SIGTERM "${FCGI_PID}"
docker stop fcgi-container
